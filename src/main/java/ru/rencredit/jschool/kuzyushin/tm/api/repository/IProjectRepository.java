package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    Project findOneByIndex(String userId, Integer index);

    Project removeById(String userId, String id);

    Project removeByName(String userId, String name);

    Project removeByIndex(String userId, Integer index);
}
