package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        this.tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task > result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        final List<Task> tasks = findAll(userId);
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        final List<Task> tasks = findAll(userId);
        for (final Task task: tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        final List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }
}
