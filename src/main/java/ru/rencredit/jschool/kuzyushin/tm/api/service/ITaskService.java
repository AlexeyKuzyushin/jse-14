package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findALl(String userId);

    void clear(String userId);

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task updateOneById(String userId, String id, String name, String description);

    Task updateOneByIndex(String userId, Integer index, String name, String description);
}
