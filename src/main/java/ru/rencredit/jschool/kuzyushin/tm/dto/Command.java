package ru.rencredit.jschool.kuzyushin.tm.dto;

public class Command {

    private String command = "";

    private String argument = "";

    private String description = "";

    public Command(String command) {
        this.command = command;
    }

    public Command(String command, String argument) {
        this.command = command;
        this.argument = argument;
    }

    public Command(String command, String argument, String description) {
        this.command = command;
        this.argument = argument;
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (command != null && !command.isEmpty()) result.append(command);
        if (argument != null && !argument.isEmpty()) result.append(", ").append(argument);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }
}
