package ru.rencredit.jschool.kuzyushin.tm.api.controller;

import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IUserController {

    void showUsers();

    void createUser();

    void showUserById();

    void removeUserById();

    void updateLogin();

    void updatePassword();

    void updateEmail();

    void updateFirstName();

    void updateLastName();

    void updateMiddleName();
}
