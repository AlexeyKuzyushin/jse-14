package ru.rencredit.jschool.kuzyushin.tm.api.service;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();
}
