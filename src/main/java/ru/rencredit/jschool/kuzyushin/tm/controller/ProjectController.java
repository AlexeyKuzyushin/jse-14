package ru.rencredit.jschool.kuzyushin.tm.controller;

import ru.rencredit.jschool.kuzyushin.tm.api.controller.IProjectController;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IAuthService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(final IProjectService projectService, final IAuthService authService) {
        this.projectService = projectService;
        this.authService = authService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findALl(userId);
        for (Project project: projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authService.getUserId();
        projectService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {

        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String userId = authService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String userId = authService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateOneById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateOneByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String userId = authService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(userId, id);
        if (project == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }
}
