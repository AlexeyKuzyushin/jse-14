package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.dto.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArguments(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();
}
