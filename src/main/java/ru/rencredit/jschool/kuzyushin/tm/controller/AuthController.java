package ru.rencredit.jschool.kuzyushin.tm.controller;

import ru.rencredit.jschool.kuzyushin.tm.api.controller.IAuthController;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IAuthService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private IAuthService authService;

    public AuthController(IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }
}
