package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void showTaskById();

    void showTaskByName();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void removeTaskByIndex();
}
