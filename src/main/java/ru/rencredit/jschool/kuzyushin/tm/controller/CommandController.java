package ru.rencredit.jschool.kuzyushin.tm.controller;

import ru.rencredit.jschool.kuzyushin.tm.api.controller.ICommandController;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ICommandService;
import ru.rencredit.jschool.kuzyushin.tm.dto.Command;
import ru.rencredit.jschool.kuzyushin.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    public void exit() {
        System.exit(0);
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alexey Kuzyushin");
        System.out.println("E-MAIL: alexeykuzyushin@yandex.ru");
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    public void showInfo() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandService.getArguments();
        System.out.println(Arrays.toString(arguments));
    }
}
