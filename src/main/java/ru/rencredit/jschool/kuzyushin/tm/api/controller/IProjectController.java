package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void showProjectById();

    void showProjectByName();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();
}
