package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();
}
