package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findALl(String userId);

    void clear(String userId);

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Project findOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    Project findOneByIndex(String userId, Integer index);

    Project removeOneById(String userId, String id);

    Project removeOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project updateOneById(String userId, String id, String name, String description);

    Project updateOneByIndex(String userId, Integer index, String name, String description);
}
