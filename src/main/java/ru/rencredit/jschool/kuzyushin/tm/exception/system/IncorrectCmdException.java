package ru.rencredit.jschool.kuzyushin.tm.exception.system;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class IncorrectCmdException extends AbstractException {

    public IncorrectCmdException(final String value) {
        super("Error! '" + value + "' is not a Task Manager command. See 'help'...");
    }
}
