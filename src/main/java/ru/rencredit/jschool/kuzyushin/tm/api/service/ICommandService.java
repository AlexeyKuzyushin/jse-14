package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getArguments();

    String[] getCommands();
}
