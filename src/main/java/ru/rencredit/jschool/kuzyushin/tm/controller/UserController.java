package ru.rencredit.jschool.kuzyushin.tm.controller;

import ru.rencredit.jschool.kuzyushin.tm.api.controller.IUserController;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.util.List;

public class UserController implements IUserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void showUsers() {
        System.out.println("[LIST USERS]");
        final List<User> users = userService.findAll();
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createUser() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-mail:");
        final String email = TerminalUtil.nextLine();
        userService.create(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void showUserById() {
        System.out.println("[SHOW USER PROFILE]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        showUser(user);
        System.out.println("[OK]");
    }

    @Override
    public void removeUserById() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.removeById(id);
        if (user == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateLogin() {
        System.out.println("[UPDATE USER LOGIN]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User userUpdated = userService.updateLogin(id, login);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updatePassword() {
        System.out.println("[UPDATE USER PASSWORD]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = userService.updatePassword(id, password);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateEmail() {
        System.out.println("[UPDATE USER EMAIL]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User userUpdated = userService.updateEmail(id, email);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateFirstName() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateFirstName(id, firstName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateLastName() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateLastName(id, lastName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateMiddleName() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final User userUpdated = userService.updateMiddleName(id, middleName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    private void showUser(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }
}
