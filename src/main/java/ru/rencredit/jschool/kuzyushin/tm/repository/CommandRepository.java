package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.constant.ArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.CommandConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.DescriptionConst;
import ru.rencredit.jschool.kuzyushin.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP, DescriptionConst.HELP
    );

    public static final Command ARGUMENTS = new Command(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, DescriptionConst.ARGUMENTS
    );

    public static final Command COMMANDS = new Command(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS, DescriptionConst.COMMANDS
    );

    public static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT, DescriptionConst.ABOUT
    );

    public static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION, DescriptionConst.VERSION
    );

    public static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO, DescriptionConst.INFO
    );

    public static final Command EXIT = new Command(
            CommandConst.EXIT, null, DescriptionConst.EXIT
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null, DescriptionConst.TASK_CREATE
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null, DescriptionConst.TASK_CLEAR
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null, DescriptionConst.TASK_LIST
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null, DescriptionConst.PROJECT_CREATE
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null, DescriptionConst.PROJECT_CLEAR
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null, DescriptionConst.PROJECT_LIST
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConst.TASK_UPDATE_BY_ID, null, DescriptionConst.TASK_UPDATE_BY_ID
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConst.TASK_UPDATE_BY_INDEX, null, DescriptionConst.TASK_UPDATE_BY_INDEX
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            CommandConst.TASK_VIEW_BY_ID, null, DescriptionConst.TASK_VIEW_BY_ID
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            CommandConst.TASK_VIEW_BY_INDEX, null, DescriptionConst.TASK_VIEW_BY_INDEX
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            CommandConst.TASK_VIEW_BY_NAME, null, DescriptionConst.TASK_VIEW_BY_NAME
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
        CommandConst.TASK_REMOVE_BY_ID, null, DescriptionConst.TASK_REMOVE_BY_ID
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConst.TASK_REMOVE_BY_INDEX, null, DescriptionConst.TASK_REMOVE_BY_INDEX
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            CommandConst.TASK_REMOVE_BY_NAME, null, DescriptionConst.TASK_REMOVE_BY_NAME
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConst.PROJECT_UPDATE_BY_ID, null, DescriptionConst.PROJECT_UPDATE_BY_ID
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConst.PROJECT_UPDATE_BY_INDEX, null, DescriptionConst.PROJECT_UPDATE_BY_INDEX
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            CommandConst.PROJECT_VIEW_BY_ID, null, DescriptionConst.PROJECT_VIEW_BY_ID
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            CommandConst.PROJECT_VIEW_BY_INDEX, null, DescriptionConst.PROJECT_VIEW_BY_INDEX
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            CommandConst.PROJECT_VIEW_BY_NAME, null, DescriptionConst.PROJECT_VIEW_BY_NAME
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConst.PROJECT_REMOVE_BY_ID, null, DescriptionConst.PROJECT_REMOVE_BY_ID
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConst.PROJECT_REMOVE_BY_INDEX, null, DescriptionConst.PROJECT_REMOVE_BY_INDEX
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            CommandConst.PROJECT_REMOVE_BY_NAME, null, DescriptionConst.PROJECT_REMOVE_BY_NAME
    );

    private static final Command LOGIN = new Command(
            CommandConst.LOGIN, null, DescriptionConst.LOGIN
    );

    private static final Command LOGOUT = new Command(
            CommandConst.LOGOUT, null, DescriptionConst.LOGOUT
    );

    private static final Command REGISTRY = new Command(
            CommandConst.REGISTRY, null, DescriptionConst.REGISTRY
    );

    private static final Command USER_LIST = new Command(
            CommandConst.USER_LIST, null, DescriptionConst.USER_LIST
    );

    private static final Command USER_VIEW_PROFILE = new Command(
            CommandConst.USER_VIEW_PROFILE, null, DescriptionConst.USER_VIEW_PROFILE
    );

    private static final Command USER_UPDATE_LOGIN = new Command(
            CommandConst.USER_UPDATE_LOGIN, null, DescriptionConst.USER_UPDATE_LOGIN
    );

    private static final Command USER_UPDATE_EMAIL = new Command(
            CommandConst.USER_UPDATE_EMAIL, null, DescriptionConst.USER_UPDATE_EMAIL
    );

    private static final Command USER_UPDATE_FIRST_NAME = new Command(
            CommandConst.USER_UPDATE_FIRST_NAME, null, DescriptionConst.USER_UPDATE_FIRST_NAME
    );

    private static final Command USER_UPDATE_LAST_NAME = new Command(
            CommandConst.USER_UPDATE_LAST_NAME, null, DescriptionConst.USER_UPDATE_LAST_NAME
    );

    private static final Command USER_UPDATE_MIDDLE_NAME = new Command(
            CommandConst.USER_UPDATE_MIDDLE_NAME, null, DescriptionConst.USER_UPDATE_MIDDLE_NAME
    );

    private static final Command USER_UPDATE_PASSWORD = new Command(
            CommandConst.USER_UPDATE_PASSWORD, null, DescriptionConst.USER_UPDATE_PASSWORD
    );

    private static final Command USER_REMOVE_BY_ID = new Command(
            CommandConst.USER_REMOVE_BY_ID, null, DescriptionConst.USER_REMOVE_BY_ID
    );

    private final String[] ARRAY_ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    private final String[] ARRAY_COMMANDS = getCommands(TERMINAL_COMMANDS);

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT, TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX, TASK_VIEW_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_REMOVE_BY_NAME, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_VIEW_BY_ID,
            PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, LOGIN, LOGOUT, REGISTRY, USER_LIST, USER_VIEW_PROFILE,
            USER_UPDATE_LOGIN, USER_UPDATE_PASSWORD, USER_UPDATE_EMAIL, USER_UPDATE_FIRST_NAME,
            USER_UPDATE_LAST_NAME, USER_UPDATE_MIDDLE_NAME, USER_REMOVE_BY_ID
    };

    public String[] getArguments(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArgument();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getCommands() {
        return ARRAY_COMMANDS;
    }

    @Override
    public String[] getArguments() {
        return ARRAY_ARGUMENTS;
    }

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getCommand();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }
}
